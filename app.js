var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,

    mongoConfiguration = require('./configuration/mongoConfiguration'),
    morgan = require('morgan'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),

    songRoutes = require('./api/routes/songRoutes'),
    generalRoutes = require('./api/routes/generalRoutes'),
    bodyParser = require('body-parser');

//created model loading here
var song = require('./api/models/songModel');
var user = require('./api/models/userModel');

// set morgan to log requests info
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.disable('etag');

// initialize body parser so we could work like human beings with the JSON
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// initialize middleware so the cookies will be handled
app.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));

mongoConfiguration();
songRoutes(app);
generalRoutes(app);

app.listen(port);
console.log('songs site RESTful API server started on port ' + port);