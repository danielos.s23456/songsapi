'use strict';
module.exports = function (app) {
    var generalController = require('../controllers/generalController');

    const path = require('path');

    // middleware function to check for logged-in users
    var sessionChecker = (req, res, next) => {
        if (req.session && req.session.user && req.cookies && req.cookies.user_sid) {
            next();
        } else {
            res.redirect('/login');
        }
    };

    app.route('/')
        .get(sessionChecker, (req, res) => {
            res.redirect('/song');
        });

    app.route('/login')
        .get((req, res) => {
            res.sendFile(path.join(__dirname, "../../public/login.html"));
        })
        .post(generalController.login);

    app.route('/logout')
        .post(generalController.logout);
};