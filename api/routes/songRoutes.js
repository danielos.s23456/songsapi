'use strict';
module.exports = function (app) {
    var songsController = require('../controllers/songController');
    var permissionController = require('../controllers/permissionController');

    var sessionChecker = (req, res, next) => {
        if (req.session && req.session.user && req.cookies && req.cookies.user_sid) {
            next();
        } else {
            res.redirect('/login');
        }
    };

    app.route('/song')
        .get(sessionChecker, songsController.listAllSongs)
        .put(sessionChecker, songsController.uploadASong);

    app.route('/song/:id')
        .get(sessionChecker, songsController.getASong)
        .delete(songsController.deleteASong);

    app.route('/song/:id/permissions/:userId')
        .put(sessionChecker, permissionController.addPermissionsToSongForUser)
        .delete(sessionChecker, permissionController.deletePermissionsToSongForUser);
};