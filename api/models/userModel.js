var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    userName: {
        type: String,
        lowercase: true,
        required: [true, "Enter username"],
        match: [/^[a-zA-Z0-9]+$/, "is invalid"],
        index: true
    },
    password: {
        type: String,
        required: [true, "Enter password"],
        match: [/^[a-zA-Z0-9]+$/, "is invalid"]
    },
    firstName: {
        type: String,
        required: [true, "Enter first name"],
    },
    lastName: {
        type: String,
        required: [true, "Enter last name"],
    },
    permittedToSongs: {type: Array}
});

module.exports = mongoose.model('User', UserSchema);