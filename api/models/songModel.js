var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SongSchema = new Schema({
    name: {
        type: String,
        required: [true, "Enter the name of the song"]
    },
    artist: {
        type: String,
        required: [true, "Enter the name of the artist"]
    },
    url: {
        type: String,
        default: "youarelame.com"
    }
});

module.exports = mongoose.model('Song', SongSchema);