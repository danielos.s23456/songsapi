'use strict';

var mongoose = require('mongoose'),
    Song = mongoose.model('Song'),
    User = mongoose.model('User'),
    permissionController = require('../controllers/permissionController');

exports.getAllUsersWithPermissionToSong = async function (songId) {
    var users = await User.find({permittedToSongs: songId},
        function (err, result) {
            if (err || !result) {
                console.log("couldnt retrieve users");
                return;
            }

            console.log("all users for the song retrieved: " + result);
            return result;
        });
    return users;
};

exports.hasPermissionsToSong = async function (songId, userId) {
    var user = await User.findById(userId, function (err, user) {
        if (err || !user) {
            console.log("no user found with id: " + userId);
            return;
        }

        return user;
    }).exec();

    var doesSongExists = (await Song.findById(songId, function (err, result) {
        if (err || !result) {
            console.log("could not check if song exists");
            return false;
        }

        return true;
    }).exec()) !== null;

    return doesSongExists && user.permittedToSongs.includes(songId);
};

exports.retrieveSongWithPermissionsCheck = async function (songId, userId) {
    var user = await User.findById(userId, function (err, user) {
        if (err || !user) {
            console.log("no user found with id: " + userId);
            return;
        }

        return user;
    });

    if (user.permittedToSongs.includes(songId)) {
        var song = await Song.findById(songId, function (err, song) {
            if (err || !song) {
                console.log("no song found");
                return;
            }

            return song;
        });
    }

    return song;
}

// ---------------api functions-----------------

exports.getASong = function (req, res) {
    if (!req.params.id) {
        console.log("request has no song id");
        res.status(400).send("request has no song id");
    }

    exports.retrieveSongWithPermissionsCheck(req.params.id, req.session.user)
        .then(function (song) {
            if (!song)
                res.status(403).send("sorry, no song for u today");

            res.json(song);
        })
};

exports.listAllSongs = function (req, res) {
    User.findById(req.session.user, function (err, user) {
        if (err) {
            console.log("no user found with id: " + req.session.user);
            res.status(403).send("sorry, no song for u today");
        }

        Song.find({_id: {$in: user.permittedToSongs}})
            .exec()
            .then((songs) => {
                res.json(songs);
            });
    });
};

exports.uploadASong = function (req, res) {
    if (!req.body._id) {
        console.log("request has no body");
        res.status(400).send("request has no data");
    }

    if (!req.body._id) {
        new Song(req.body).save(function (err, song) {
            if (err || !song) {
                console.log("could't save song");
                return;
            }

            permissionController.addPermissionsToSong(song.id, req.session.user)
                .then((result) => {
                    if (result) res.json("song saved successfully");
                });
        });
        return;
    }

    exports.hasPermissionsToSong(req.params.id, req.session.user)
        .then(function (hasPermissions) {
            if (!hasPermissions) {
                res.status(403).send("sorry, no song for u today");
                return;
            }

            Song.findOneAndUpdate({_id: req.body._id},
                req.body,
                {new: true},
                function (err, song) {
                    if (err || !song) {
                        console.log("song update failed");
                        res.status(500).send("song update failed");
                        return;
                    }

                    res.send("song update successfully");
                });
        });
};

exports.deleteASong = function (req, res) {
    if (!req.params.id)
        res.send("bad request");

    Song.find({
        _id: req.params.id
    }, function (err, song) {
        if (err || !song) {
            console.log("could't remove song");
            return;
        }

        exports.getAllUsersWithPermissionToSong(req.params.id).then(
            function (users) {
                if (!users) {
                    console.log("no song found");
                    return;
                }

                if (users.filter(function (user) {
                    permissionController.deletePermissionsToSong(req.params.id, user.id)
                        .then((result) => {
                            if (!result) {
                                console.log("song permissions failed to be deleted for user " + user.id);
                                return false;
                            }

                            console.log("song permissions successfully deleted for user " + user.id);
                            return true;
                        });
                }).length === users.length) {
                    res.json("song deleted successfully");
                    return true;
                }

                res.status(500).send("failed to delete song properly");
            }
        );
    });

};