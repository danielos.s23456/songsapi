'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User');

exports.updateUser = function (user) {
    User.findOneAndUpdate({_id: user.id},
        user,
        {new: true},
        function (err, result) {
            if (err || !result) {
                console.log("user update failed");
                return false;
            }

            console.log("user updated successfully");
            return true;
        });
}