'use strict';

var mongoose = require('mongoose'),
    Song = mongoose.model('Song'),
    User = mongoose.model('User'),
    songController = require('../controllers/songController'),
    userController = require('../controllers/userController');

exports.addPermissionsToSong = async function (songId, userId) {
    Song.findById(songId).then(function (song) {
        if (!song) {
            console.log("could't find user for permission addition");
            return;
        }

        User.findById(userId).then(function (user) {
            if (!user) {
                return false;
            }

            if (!user.permittedToSongs.includes(songId)) {
                user.permittedToSongs.push(songId);
            }

            return userController.updateUser(user);
        });
    });
};

exports.deletePermissionsToSong = async function (songId, userId) {
    User.findById(userId).then(function (user) {
        if (!user) {
            console.log("could't find user for permission removal");
            return;
        }

        return user.permittedToSongs.filter(function (iteratedSongId, index) {
            if (iteratedSongId === songId) {
                user.permittedToSongs.splice(index, 1);
                userController.updateUser(user);
                return true;
            }
            return false;
        }).length > 0;
    });
};

exports.addPermissionsToSongForUser = function (req, res) {
    songController.hasPermissionsToSong(req.params.id, req.session.user).then(async function (result) {
        if (result) {
            if (await exports.addPermissionsToSong(req.params.id, req.params.userId))
                res.send("permissions to this song for a given user were added");
            else
                res.send("couldn't add permissions to this song for a given user");
        } else {
            if (result === undefined)
                console.log("could not retrieve permission for current user");
            res.send("no permissions to this song for current user");
        }
    });
};

exports.deletePermissionsToSongForUser = function (req, res) {
    songController.hasPermissionsToSong(req.params.id, req.session.user).then(async function (result) {
        if (result) {
            if (await exports.deletePermissionsToSong(req.params.id, req.params.userId))
                res.send("permissions to this song for a given user were deleted");
            else
                res.send("couldn't delete permissions to this song for a given user");
        } else {
            if (result === undefined)
                console.log("could not retrieve permission for current user");
            res.send("no permissions to this song for current user");
        }
    });
};