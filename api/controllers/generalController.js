'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User');

exports.login = function (req, res) {
    User.findOne({
        userName: req.body.username,
        password: req.body.password
    }, function (err, user) {
        if (!user) {
            res.redirect('/login');
        } else {
            req.session.user = user.id;
            res.redirect('/');
        }
    });
};

exports.logout = function (req, res) {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
    }
    res.redirect('/login');
};